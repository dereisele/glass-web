var offset = 0;

$(document).ready(function() {
  loadArtists(offset);
  offset = offset + 10;
  $("#load_more_btn").click(function() {
    loadArtists(offset);
    offset = offset + 10;
  })
});

function loadArtists(offset) {
  $.getJSON("/artists?offset=" + offset, function(data) {
    var artist = data.result;
    artist.forEach(function(value, index) {
      if (index == 0) {
        var myDiv = $("#artist_div");
      } else {
        var myDiv = $("#artist_div").clone();
      }
      myDiv.find(".artist_name").text(value.name)
      myDiv.click({
        id: value.id
      }, openArtist);
      console.log(value.name)
      myDiv.appendTo("#artists");
    });
  });
}

function openArtist(event) {
  alert(event.data.id);
}