$(document).ready(function() {
  id = getURLParameter("id");
  console.log("id:" + id)
  loadAlbum(id);
});

function loadAlbum(offset) {
  $.getJSON("/albums/" + id, function(data) {
    var album = data.result;
    $("title, h1").text(album.name + " | GLASS")
    album.songs.forEach(function(value, index) {
      if (index == 0) {
        var myDiv = $("#song_div");
      } else {
        var myDiv = $("#song_div").clone();
      }
      myDiv.find(".song_name").text(value.name)
      myDiv.click({
        id: value.id
      }, playSongEvent);
      console.log(value.name)
      myDiv.appendTo("#songs");
    });
  });
}

function playSongEvent(event){
  console.log("Play id:" + event.data.id);
  loadSong(event.data.id)
}

function loadSong(id){
  $.getJSON("/songs/" + id, function(data){
    var url = data.result.files[0].path;
    var audio = $("#player");
    $("#player_source").attr("src", url).appendTo(audio);
    $("#player_source").attr("type", "audio/" + getExtension(url))
    audio[0].pause();
    audio[0].load();
    audio[0].play();
  })
}
